<?php
include('config/koneksi.php');
$config['judul_sub_halaman'] = "Dashboard";
$config['hal_aktif'] = "home";
if(!isset($_SESSION['level'])){
  header('Location: login.php');
}
$vws->set_inline("<link rel='stylesheet' href='assets/css/leaflet.css'/>");
$vws->set_inline("<link rel='stylesheet' href='assets/css/MarkerCluster.css'/>");
$vws->set_inline("<link rel='stylesheet' href='assets/css/MarkerCluster.Default.css'/>");
include('header.php');
$vws->reset_inline();
?>
<style>
.info {
			padding: 6px 8px;
			font: 14px/16px Arial, Helvetica, sans-serif;
			background: white;
			background: rgba(255,255,255,0.8);
			box-shadow: 0 0 15px rgba(0,0,0,0.2);
			border-radius: 5px;
   min-height: 80px;
   min-width: 200px;
		}
		.info h4 {
			margin: 0 0 5px;
			color: #777;
		}

		.legend {
   padding: 6px 8px;
			font: 14px/16px Arial, Helvetica, sans-serif;
			background: white;
			background: rgba(255,255,255,0.8);
			box-shadow: 0 0 15px rgba(0,0,0,0.2);
			border-radius: 5px;
			text-align: left;
			line-height: 18px;
			color: #555;
		}
		.legend i {
			width: 18px;
			height: 18px;
			float: left;
			opacity: 0.7;
		}
</style>
  <h2>Selamat Datang di <?= $config['nama_website'] ?></h2>
  <div id="map" style="width: 70vw; height:70vh"></div>
<?php 
ob_start();
?>
<script src="assets/js/leaflet.js"></script>
<script src="assets/js/leaflet.markercluster.js"></script>
<!--script type="text/javascript" src="coba2.php"></script>
<script type="text/javascript" src="camat_geojson.php"></script-->

<!--/ custom javascripts -->
<script>
var currentSelectedCategory = [];
function sidebarClick(elem) {
  map.setView([$(elem).attr('lat'), $(elem).attr('lng')], 17);
  layer = markerClusters.getLayer($(elem).data('id'));
  layer.fire('click');
}
</script>
<script>
var mapState = "1";
var currentDataYear = '2015';
var jenis_url = {
	'klinik dots': {
		url: 'assets/images/klinik.png',
		size: [28, 27]
	},
	'Dokter Swasta': {
		url: 'assets/images/dokter_swasta.png',
		size: [28, 36]
	},
	'Pos TB desa': {
		url: 'assets/images/posdesa.png',
		size: [32, 32]
	}
};
function greenIcon(feature){
	//console.log(jenis_url[feature.properties.jenis].url);
	return L.icon({
    	iconUrl: jenis_url[feature.properties.jenis].url,
    	iconSize: jenis_url[feature.properties.jenis].size,
     popupAnchor: [0, -25]// size of the icon 
	});	
};

var brewerI = ['#f7fbff','#deebf7','#c6dbef','#9ecae1','#6baed6','#4292c6','#2171b5','#084594'];
var brewerT = ['#800026','#BD0026','#E31A1C','#FC4E2A','#FD8D3C','#FEB24C','#FED976','#FFEDA0'];
var warnaData = ['#BD0026','#FEB24C','#81F048'];

function getColor(d) {
			return d > 1600 ? brewerT[0] :
			       d > 1400  ? brewerT[1] :
			       d > 1200  ? brewerT[2] :
			       d > 1000  ? brewerT[3] :
			       d > 800   ? brewerT[4] :
			       d > 600   ? brewerT[5] :
			       d > 400   ? brewerT[6] :
			                  brewerT[7];
		}

		function getColorBta(d){
			return d < 35 ? warnaData[0] : d < 53  ? warnaData[1] :warnaData[2];
		};
		function getColorSrate(d){
			return d > 90 ? warnaData[2] : d > 68  ? warnaData[1] :warnaData[0];
		};
		function getColorScase(d){
			return d < 68 ? warnaData[0] : d < 90  ? warnaData[1] :warnaData[2];
		};
		function getColorObatLkp(d){
			return d > 100 ? warnaData[2] : d > 50  ? warnaData[1] :warnaData[0];
		};
		function getColorBta_camat(d){
			return d < 35 ? warnaData[0] : d < 53  ? warnaData[1] :warnaData[2];
		};
		function getColorSrate_camat(d){
			return d > 90 ? warnaData[2] : d > 68  ? warnaData[1] :warnaData[0];
		};
		function getColorScase_camat(d){
			return d < 68 ? warnaData[0] : d < 90  ? warnaData[1] :warnaData[2];
		};
		function getColorObatLkp_camat(d){
			return d > 100 ? warnaData[2] : d > 50  ? warnaData[1] :warnaData[0];
		};

		function style(feature) {
			return {
				weight: 2,
				opacity: 1,
				color: 'white',
				dashArray: '3',
				fillOpacity: 0.3,
				fillColor: (feature.properties.jenis == 'kabupaten') ? getColorBta(feature.properties.btapositif[currentDataYear]) : getColorBta_camat(feature.properties.btapositif[currentDataYear])
			};
		};
		function styleBta(feature) {
			return {
				weight: 2,
				opacity: 1,
				color: 'white',
				dashArray: '3',
				fillOpacity: 0.3,
				fillColor: (feature.properties.jenis == 'kabupaten') ? getColorBta(feature.properties.btapositif[currentDataYear]) : getColorBta_camat(feature.properties.btapositif[currentDataYear])
			};
		};
		function styleSrate(feature) {
			return {
				weight: 2,
				opacity: 1,
				color: 'white',
				dashArray: '3',
				fillOpacity: 0.3,
				fillColor: (feature.properties.jenis == 'kabupaten') ? getColorSrate(feature.properties.succesrate[currentDataYear]) : getColorSrate_camat(feature.properties.succesrate[currentDataYear])
			};
		};
		function styleScase(feature) {
			return {
				weight: 2,
				opacity: 1,
				color: 'white',
				dashArray: '3',
				fillOpacity: 0.3,
				fillColor: (feature.properties.jenis == 'kabupaten') ? getColorScase(feature.properties.semuakasus[currentDataYear]) : getColorScase_camat(feature.properties.semuakasus[currentDataYear])
			};
		};
		function styleObatLkp(feature) {
			return {
				weight: 2,
				opacity: 1,
				color: 'white',
				dashArray: '3',
				fillOpacity: 0.3,
				fillColor: (feature.properties.jenis == 'kabupaten') ? getColorObatLkp(feature.properties.pengobatanlengkap[currentDataYear]) : getColorObatLkp_camat(feature.properties.pengobatanlengkap[currentDataYear])
			};
		};
  function highlightFeature(e) {
			var layer = e.target;

			layer.setStyle({
				weight: 5,
				color: '#666',
				dashArray: '',
				fillOpacity: 0.7
			});

			if (!L.Browser.ie && !L.Browser.opera) {
				layer.bringToFront();
			}

			info.update(layer.feature.properties);
		};
  
  function resetHighlight(e) {
			if(mapState == '1'){
				geojson_riau.setStyle(styleBta);
    geojson_camat.setStyle(styleBta);
			} else if(mapState == '2'){
				geojson_riau.setStyle(styleScase);
    geojson_camat.setStyle(styleScase);
			} else if(mapState == '3'){
				geojson_riau.setStyle(styleSrate);
    geojson_camat.setStyle(styleSrate);
			} else {
				geojson_riau.setStyle(styleObatLkp);
    geojson_camat.setStyle(styleObatLkp);
			}
			//geojson.resetStyle(e.target);
			info.update();
		};

		function zoomToFeature(e) {
			map.fitBounds(e.target.getBounds());
		};

		function onEachFeature(feature, layer) {
			layer.on({
				mouseover: highlightFeature,
				mouseout: resetHighlight,
				click: zoomToFeature
			});
		};
  function syncCategoryIcon(){
  Object.keys(markerRumahSakit._layers).forEach(function(id) {
   thisMarker = markerRumahSakit._layers[id].feature.properties;
   isSelected = Object.values(currentSelectedCategory).find(function(el) {return el == thisMarker.jenis});
   console.log(isSelected);
   if(typeof isSelected === 'undefined'){
    $(markerRumahSakit._layers[id].getElement()).addClass('hidden');
   } else {
    $(markerRumahSakit._layers[id].getElement()).removeClass('hidden');
   }
  });
 };
 function zoomToFeature(e) {
  map.fitBounds(e.target.getBounds());
 }

 function onEachFeature(feature, layer) {
  layer.on({
   mouseover: highlightFeature,
   mouseout: resetHighlight,
   click: zoomToFeature
  });
 }
 var geojson_riau = L.geoJson(null, {
			style: style,
			onEachFeature: onEachFeature
	});
 $.getJSON("coba2.php", function (data) {
  geojson_riau.addData(data);
 });

 var geojson_camat = L.geoJson(null, {
  style: style,
  onEachFeature: onEachFeature
 });
 $.getJSON("camat_geojson.php", function (data) {
  geojson_camat.addData(data);
 });
 
 var markerRumahSakitLayer = L.geoJson(null);
 var markerRumahSakit = L.geoJson(null, {
  pointToLayer: function(feature, latlng){
   return L.marker(latlng, {
    title: feature.properties.namaklinik, 
    icon: greenIcon(feature),
    opacity: 1,
    riseOnHover: true
   });
  },
  onEachFeature: function(feature, layer){
   var name = feature.properties.namaklinik;
   layer.bindPopup(name);
  }
 });
 $.getJSON("getdata2.php", function (data) {
  markerRumahSakit.addData(data);
  map.addLayer(markerRumahSakitLayer);
 });
 var googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
     maxZoom: 20,
     subdomains:['mt0','mt1','mt2','mt3']
 });

 var osm = L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
   maxZoom: 20,
   subdomains: ['a' , 'b' , 'c'],
   attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>" '
 });
 var markerClusters = new L.MarkerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: false,
  zoomToBoundsOnClick: true,
  disableClusteringAtZoom: 16
 });
 var map = L.map('map', {
   zoom: 8,
   center: [0.4, 102],
   zoomControl: false,
   layers: [osm, geojson_riau, geojson_camat, markerClusters],
   attributionControl: true
 });
 
 map.on("overlayadd", function (e) {
   markerClusters.addLayer(markerRumahSakit);
 });

 map.on("overlayremove", function (e) {
   markerClusters.removeLayer(markerRumahSakit);
 });
 
 map.on("zoomend", function(e) {
  syncCategoryIcon();
 });
 
 map.attributionControl.addAttribution('Data TB &copy; <a href="http://dinkes.riauprove.go.id/">Dinas Kesehatan Provinsi Riau</a>, ' + '');
 
 markerClusters.on('animationend', function(e) {
  syncCategoryIcon();
 });
 
 var info = L.control();

 info.onAdd = function (map) {
  this._div = L.DomUtil.create('div', 'info');
  this.update();
  return this._div;
 };

 info.update = function (props) {
  this._div.innerHTML = '<h4>BTA Positive di Riau</h4>' +  (props ?
   '<b>' + props.kabupaten + '</b><br />' + (props.btapositif[currentDataYear]?props.btapositif[currentDataYear]:0) + ' orang'
   : 'Letakkan kursor pada peta');
 };
 info.addTo(map);
 var legend = L.control({position: 'bottomright'});
 var kontroldata = L.control({position: 'bottomleft'});
 kontroldata.onAdd = function(map){
  var div = L.DomUtil.create('div', 'info legend');
  div.innerHTML = "<select name='tahun' class='form-control' id='tahundata'><option selected disabled>Pilih Tahun...</option><?php
  foreach(range(2015,date('Y')) as $th){ 
   echo "<option value='".$th."'>".$th."</option>"; 
  } ?></select>";
  pilihan = ['<label for="data1"><input type="radio" name="data" id="data1" value="1" checked/> BTA Positif</label>','<label for="data2"><input type="radio" name="data" id="data2" value="2"/> Semua Kasus</label>','<label for="data3"><input type="radio" id="data3" name="data" value="3"/> Success Rate</label>','<label for="data4"><input type="radio" name="data" id="data4" value="4"/> Pengobatan Lengkap</label>'];
  div.innerHTML += pilihan.join('<br>');
  return div;
 };
 kontroldata.addTo(map);
 
 L.Control.Title = L.Control.extend({
  onAdd: function(map){
   var divlegend = L.DomUtil.create('div','kotaktahun');
   divlegend.style = "background-color:rgba(255,255,255,1);padding:10px;min-width: 250px";
   divlegend.innerHTML = "<h4 class='pull-left'>Jenis Klinik</h4><button type='button' class='btn btn-md btn-primary pull-right' data-toggle='collapse' data-target='#pilihjenis_list'><span class='fa fa-chevron-down'><span></button><br>";
   divlegend.innerHTML += '<form name="pilihjenis" id="pilihjenis" class="pull-left">'
   +'<ul class="collapse list-group" id="pilihjenis_list" style="height:60vh;width:220px;max-height:60vh;color: #5a5a5a">'
    <?php foreach($config['jenis_klinik'] as $key=>$val){ ?>
    +'<li class="list-group-item">'
    +'<input type="checkbox" name="jenis_klinik" id="jenis_<?= $key ?>" value="<?= $key ?>" checked />&nbsp;&nbsp;<label for="jenis_<?= $key ?>"><img src="'
    +jenis_url['<?= $key ?>']['url']+'" style="max-width: 15px !important;" /><?= $val ?></label><br>'
    +'</li>'
    <?php } ?>
   +'</ul></form>';
   L.DomEvent.disableClickPropagation(divlegend);
   L.DomEvent.disableScrollPropagation(divlegend);
   return divlegend;
  }
 });
 L.control.title = function(opts){
  return new L.Control.Title(opts);
 }
 L.control.title({position: 'topleft'}).addTo(map);

 legend.onAdd = function (map) {

  var div = L.DomUtil.create('div', 'legend'),
   //grades = [0, 10, 20, 50, 100, 200, 500, 1000],
   grades = ['Buruk','Sedang','Baik'],
   labels = [],
   from, to;
  div.innerHTML += "<strong style='float:left'>Keterangan : &nbsp;</strong>";
  for (var i = 0; i < grades.length; i++) {
   //from = grades[i];
   //to = grades[i + 1];
   
   labels.push(
    '<div style="float:left;margin-right:8px"><i style="background:' + warnaData[i] + '"></i> ' +
    grades[i])
  }

  div.innerHTML += labels.join('</div>');
  div.innerHTML += "</div>";
  return div;
 };

 legend.addTo(map);
 $("input[name=data]").on('change', function() {
  $this = $(this);
  atribut = $this.val();
  if(atribut == 1){
   geojson_riau.setStyle(styleBta);
   geojson_camat.setStyle(styleBta);
   mapState = "1";
   map.removeControl(info);
   info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
   };
   info.update = function (props) {
    this._div.innerHTML = '<h4>BTA Positive di Riau</h4>' +  (props ?
     '<b>' + props.kabupaten + '</b><br />' + (props.btapositif[currentDataYear]?props.btapositif[currentDataYear]:0) + ' orang'
     : 'Letakkan mouse Anda pada peta');
   };
   info.addTo(map);
  } else if(atribut == 2){
   geojson_riau.setStyle(styleScase);
   geojson_camat.setStyle(styleScase);
   mapState = "2";
   map.removeControl(info);
   info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
   };
   info.update = function (props) {
    this._div.innerHTML = '<h4>Semua Kasus di Riau</h4>' +  (props ?
     '<b>' + props.kabupaten + '</b><br />' + (props.semuakasus[currentDataYear]?props.semuakasus[currentDataYear]:0) + ' orang'
     : 'Letakkan mouse Anda pada peta');
   };
   info.addTo(map);
  } else if(atribut == 3){
   geojson_riau.setStyle(styleSrate);
   geojson_camat.setStyle(styleSrate);
   mapState = "3";
   map.removeControl(info);
   info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
   };
   info.update = function (props) {
    this._div.innerHTML = '<h4>Success Rate di Riau</h4>' +  (props ?
     '<b>' + props.kabupaten + '</b><br />' + (props.succesrate[currentDataYear]?props.succesrate[currentDataYear]:0) + '%'
     : 'Letakkan mouse Anda pada peta');
   };
   info.addTo(map);
  } else {
   geojson_riau.setStyle(styleObatLkp);
   geojson_camat.setStyle(styleObatLkp);
   mapState = "4";
   map.removeControl(info);
   info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
   };
   info.update = function (props) {
    this._div.innerHTML = '<h4>Pengobatan Lengkap di Riau</h4>' +  (props ?
     '<b>' + props.kabupaten + '</b><br />' + (props.pengobatanlengkap[currentDataYear]?props.pengobatanlengkap[currentDataYear]:0) + ' orang'
     : 'Letakkan mouse Anda pada peta');
   };
   info.addTo(map);
  }
 });
 var baseLayers = {
   "Open Street Map": osm,
   "Google Streets": googleStreets,
  };
  var overLayers = {
    "Titik Rumah Sakit": markerRumahSakitLayer
  }
  if (document.body.clientWidth <= 767) {
   var isCollapsed = true;
  } else {
   var isCollapsed = false;
  }
  var layerControl = L.control.layers(baseLayers,overLayers, {
    collapsed: isCollapsed,
    position: 'bottomright'
  }).addTo(map);
</script>
<script>
currentViewState = 0;
$(document).ready(function() {
  selectedVal = $("#pilihjenis").serializeArray();
  Object.keys(selectedVal).forEach(function(key) {
   currentSelectedCategory[key] = selectedVal[key]['value'];
  });
  $("#tahundata").on('change', function() {
   currentDataYear = $(this).val();
   resetHighlight();
  });
  $("#pilihjenis").on('change', function() {
   currentSelectedCategory = [];
   selectedVal = $(this).serializeArray();
   Object.keys(selectedVal).forEach(function(key) {
    currentSelectedCategory[key] = selectedVal[key]['value'];
   });
   syncCategoryIcon();
  });
});
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');
?>