<?php
header('Content-type: text/json');
include('config/koneksi.php');
$query = mysqli_query($kon, "select * from dots");
$geojson = [
 "type" => "FeatureCollection",
 "crs"  => [
  "type"=> "name",
  "properties" => ["name"=>"urn:ogc:def:crs:OGC:1.3:CRS84" ]
 ], 
 "features" => []
];
while($row=mysqli_fetch_assoc($query)){
 $features=[
  "type" => "Feature",
  "properties" => [
   "idklinik"=> $row['idklinik'],
   "namaklinik" => $row['namaklinik'],
   "jenis" => $row['jenis']
  
  ],
  "geometry" => [
   "type" => "Point",
   "coordinates" => [$row['longitude'],$row['latitude']]
  ]
 ];
 $geojson['features'][] = $features;
}
echo json_encode($geojson);
?>

