<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "Kelola Data TB Kota Pekanbaru";
$config['hal_aktif'] = "tbkel";
$tahun = isset($_GET['tahun'])?$_GET['tahun']:'2015';
hak_akses(['admin'],TRUE);
ob_start();
?>
<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/jquery.dataTables_themeroller.min.css" rel="stylesheet"/>
<?php
$vws->set_inline(ob_get_clean());
include('header.php');
$vws->reset_inline();
?>
<style>
 hr{
  border: 1px solid #7a7a7a;
  margin: 5px;
 }
 .print-only { display: none }
 @media print{
  nav, #content-title, .hidden-print { display: none }
  .print-only { display: block }
 }
</style>
<div class="col-sm-12">

 <div class="btn-group hidden-print" style="margin:25px 5px">
  <a href="form_tbkel.php?tahun=<?= $tahun ?>" class="btn btn-primary btn-md"><span class="fa fa-plus"></span> Tambah Data</a>
  <button type="button" class="btn btn-primary btn-md" id="printhasil"><span class="fa fa-file-pdf-o"></span> Unduh Laporan</a>
 </div>
 <div class="btn-group hidden-print" style="float:right;margin:25px 5px">
  <form method="GET" id="tahundata">
   <select name="tahun" class="form-control" onchange="$('#tahundata').submit()">
    <option selected disabled>Pilih Tahun Data...</option>
    <?php
    foreach(range(2015, date('Y')) as $th){
    ?>
    <option value='<?= $th ?>' <?= ($th == $tahun)?"selected":"" ?>><?= $th ?></option>
    <?php } ?>
   </select>
  </form>
 </div>

 <table class="table table-striped table-bordered table-responsive">
  <thead>
   <tr>
    <th rowspan="2">Kecamatan</th>
    <th colspan="4" class="text-center">Jumlah</th>
    <th class="hidden-print text-center" rowspan="2">Aksi</th>
   </tr>
   <tr>
    <th class="text-center">BTA Positif</th>
    <th class="text-center">Semua Kasus</th>
    <th class="text-center">Success Rate</th>
    <th class="text-center">Pengobatan Lengkap</th>
   </tr>
  </thead>
  <tbody>
   <?php 
   $query = mysqli_query($kon, "select *,kecamatan.kecamatan from datatbkota,kecamatan where datatbkota.kecamatan = kecamatan.id_kecamatan and tahun  = '".$tahun."'");
   $rows = [];
   if(mysqli_num_rows($query) !== 0){
    while($row=mysqli_fetch_assoc($query)){ 
    $rows[] = array(
      'kec'=> $row['kecamatan'],
      'bta'=> $row['btapositif'],
      'all'=> $row['semuakasus'],
      'srt'=> $row['succesrate']."%",
      'obt'=> $row['pengobatanlengkap']
    );
    ?>
    <tr>
     <td><?= $row['kecamatan'] ?></td>
     <td class="text-center"><?= $row['btapositif'] ?></td>
     <td class="text-center"><?= $row['semuakasus'] ?></td>
     <td class="text-center"><?= $row['succesrate'] ?></td>
     <td class="text-center"><?= $row['pengobatanlengkap'] ?></td>
     <td class="hidden-print text-center"><a href="form_tbkel.php?edit=<?= $row['idtbkota'] ?>"><span class="fa fa-pencil"></span> Ubah</a></td>
    </tr>
    <?php }
    } else {
     ?>
     <tr>
      <td colspan="6" class="text-center">Tidak Ada Data</td>
     </tr>
    <?php } ?>
  </tbody>
 </table>
</div>
<?php
ob_start();
?>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jspdf.min.js"></script>
<script src="assets/js/jspdf.plugin.autotable.min.js"></script>
<script>
  var cols = [
    {title: "Kecamatan", dataKey: 'kec'},
    {title: "BTA Positif", dataKey: 'bta'},
    {title: "Semua Kasus", dataKey: 'all'},
    {title: "Tingkat Kesuksesan", dataKey: 'srt'},
    {title: "Pengobatan Lengkap", dataKey: 'obt'}
  ];
  var rows = <?= json_encode($rows) ?>;
  $(document).ready(function() {
    $("#printhasil").on('click', function() {
    var doc = new jsPDF({
     orientation: 'l',
     unit: 'pt',
     format: 'a4'
    });
    doc.autoTable(cols, rows, {
     theme: 'grid',
     margin: {top: 100},
     styles: {halign:'center'},
     addPageContent: function(data) {
       doc.setFontSize(14);
       doc.text("Dinas Kesehatan Provinsi Riau", 40, 30);
       doc.text("Laporan Data TB Kota Pekanbaru 2016", 320, 70);
       doc.setLineWidth(1.5);
       doc.setDrawColor(50,50,50);
       doc.line(40,80,800,80);
     }
    });
    doc.save("laporan-data.pdf");
   });
  });
  
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');