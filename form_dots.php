<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "Tambah Data DOTS";
$config['hal_aktif'] = "dots";
$jenis_klinik = $config['jenis_klinik'];
hak_akses(['admin'],TRUE);
if(isset($_GET['edit'])){
 $config['judul_sub_halaman'] = "Ubah Data DOTS";
 $query = mysqli_query($kon, "select * from dots where idklinik='".$_GET['edit']."'");
 $row = mysqli_fetch_assoc($query);
} else if (isset($_POST['id'])){
 if($_POST['id'] === ''){
  $query = mysqli_query($kon, "insert into dots values('','".$_POST['namaklinik']."','".$_POST['jenis']."','".$_POST['geometlng']."','".$_POST['geomet']."')");
  if($query){
   echo "<script>alert('Data DOTS berhasil ditambahkan!');\n document.location = 'lihat_dots.php'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan Penambahan.');\n document.location = 'lihat_dots.php'</script>";
  }
 } else {
  //echo "update dots set namaklinik='".$_POST['namaklinik']."',jenis='".$_POST['jenis']."',longitude='".$_POST['geometlng']."',latitude='".$_POST['geomet']."' where idklinik='".$_POST['id']."'";
  $query = mysqli_query($kon,"update dots set namaklinik='".$_POST['namaklinik']."',jenis='".$_POST['jenis']."',longitude='".$_POST['geometlng']."',latitude='".$_POST['geomet']."' where idklinik='".$_POST['id']."'");
  if($query){
   echo "<script>alert('Data berhasil diperbarui');\n document.location = 'lihat_dots.php'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan dalam pembaruan data.');\n document.location = 'lihat_dots.php'</script>";
  }
 }
}
$vws->set_inline("<link rel='stylesheet' href='assets/css/leaflet.css'/>");
include('header.php');
$vws->reset_inline();
?>
<form method="POST" action="form_dots.php" name="pengguna" class="form-horizontal">
 <input type="hidden" name="id" value="<?= isset($row['idklinik'])?$row['idklinik']:'' ?>" />
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="namaklinik">Nama Klinik</label>
   </div>
   <div class="col-sm-8">
    <input type="text" name="namaklinik" id="namaklinik" class="form-control" value="<?= isset($row['namaklinik'])?$row['namaklinik']:'' ?>" placeholder="Nama Klinik" maxlength="50" required />
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="jenis">Jenis Klinik</label>
   </div>
   <div class="col-sm-8">
    <select name="jenis" id="jenis" class="form-control">
     <?php foreach($jenis_klinik as $key=>$value){ ?>
     <option value="<?= $key ?>" <?= isset($row['jenis'])?($row['jenis'] == $key?"selected":""):"" ?>><?= $value ?></option>
     <?php } ?>
    </select>
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="geomet">Koordinat</label>
   </div>
   <div class="col-sm-8">
    <a href="#petaform" data-toggle="modal" class="btn btn-login">Buka Peta</a><br><br>
    <input type="text" name="geomet" id="geomet" class="form-control" value="<?= isset($row['latitude'])?$row['latitude']:'' ?>" placeholder="Latitude" required />
    <input type="text" name="geometlng" id="geometlng" class="form-control" value="<?= isset($row['longitude'])?$row['longitude']:'' ?>" placeholder="Longitude" required />
   </div>
  </div>
  
  <div class="form-group">
   <div class="col-sm-12">
    <button type="submit" class="form-control btn btn-login" name="simpan">Simpan</button>
   </div>
  </div>
</form>
<div id="petaform" class="modal" role="dialog">
 <div class="modal-dialog">
  <div class="panel panel-primary">
   <div class="panel-heading">Pilih Koordinat Peta</div>
   <div class="panel-body">
    <h5 style="margin:0;text-align: center">Koordinat Terpilih: <span id="koord"></span></h5>
    <div id="map" style="height: 500px"></div>
   </div>
   <div class="panel-footer">
    <button class="btn btn-primary" data-dismiss="modal">Selesai</button>
   </div>
  </div> 
 </div>
</div>
<?php
ob_start();
?>
<script src="assets/js/leaflet.js"></script>
<!--/ custom javascripts -->
<script>
<?php 
echo "var currentLatLng = [".(isset($row['latitude'])?$row['latitude'].",".$row['longitude']:'0,0')."]; \n";
?>
var address = '';
var kecamatanColors = {
	"Bukit Raya": "rgba(209,229,240,1.0)",
	"Lima Puluh": "rgba(214,96,77,1.0)",
	"Marpoyan Damai": "rgba(244,165,130,1.0)",
	"Payung Sekaki": "rgba(253,219,199,1.0)",
	"Pekanbaru": "rgba(147,147,147,1.0)",
	"Rumbai": "rgba(178,24,43,1.0)",
	"Rumbai Pesisir": "rgba(146,197,222,1.0)",
	"Sail": "rgba(103,0,31,1.0)",
	"Senapelan": "rgba(67,147,195,1.0)",
	"Sukajadi": "rgba(33,102,172,1.0)",
	"Tampan": "rgba(5,48,97,1.0)",
	"Tenayan Raya": "rgba(159,78,209,1.0)"
};

 /** fungsi untuk style kelurahan dikategorikan ke kecamatan
  */
 function style_kelurahan(feature) {
  return {
   opacity: 1,
   color: kecamatanColors[feature.properties['kabupaten']],
   dashArray: '',
   lineCap: 'butt',
   lineJoin: 'miter',
   weight: 2.0,
   fillOpacity: 0.3,
   fillColors: kecamatanColors[feature.properties['kabupaten']],
  };
 }
 
 var pekanbaru = L.geoJson(null, {
   style: style_kelurahan,
   onEachFeature: function (feature, layer) {
    //layer.bindPopup("<strong>"+feature.properties.kecamatan+"</strong>");
    layer.on('click', function() {
     markerForm.setLatLng([lat,lng]).addTo(map);
     $("#koord").text(lat+" ,"+lng);
     $("#geomet").val(lat);
     $("#geometlng").val(lng);
     $("#kecamatan").val(feature.properties.id);
    });
  }
  });
 $.getJSON("camat_geojson.php", function (data) {
  pekanbaru.addData(data);
 });
var osm = L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
   maxZoom: 20,
   subdomains: ['a' , 'b' , 'c'],
   attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>" '
   });
var map = L.map("map", {
   zoom: 11,
   center: [0.555, 101.38],
   layers: [osm, pekanbaru],
   zoomControl: false,
   attributionControl: true
  });
markerForm = L.marker(currentLatLng).addTo(map);
$("#koord").text(currentLatLng[0]+" ,"+currentLatLng[1]);
map.on("click", function(ev) {
 console.log(lat+" "+lng);
});
map.on("mousedown", function (ev) {
	lat = ev.latlng.lat;
	lng = ev.latlng.lng;
});
$('#petaform').on('shown.bs.modal', function(){
  map.invalidateSize();
 });
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');