<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "Tambah Data TB Kabupaten/Kota";
$config['hal_aktif'] = "tbkab";
hak_akses(['admin'],TRUE);
if(isset($_GET['edit'])){
 $config['judul_sub_halaman'] = "Ubah Data TB Kabupaten/Kota";
 $query = mysqli_query($kon, "select *,kabupaten.kabupaten from datatb,kabupaten where datatb.kabupaten = kabupaten.id_kabupaten and id_tb='".$_GET['edit']."'");
 $row = mysqli_fetch_assoc($query);
} else if(isset($_POST['id_tb'])){
  if($_POST['id_tb'] == ''){
   $query = mysqli_query($kon,"insert into datatb values ('','".$_POST['tahun']."','".$_POST['btapositif']."','".$_POST['semuakasus']."','".$_POST['succesrate']."','".$_POST['pengobatanlengkap']."','".$_POST['kabupaten']."','".date('Y-m-d')."')");
   if($query){
    echo "<script>alert('Data TB Kabupaten berhasil ditambahkan');\n document.location = 'lihat_tbkab.php?tahun=".$_POST['tahun']."'</script>";
   } else {
    echo mysqli_error($kon);
    echo "<script>alert('Terdapat Kesalahan dalam penambahan data.');\n document.location = 'lihat_tbkab.php?tahun=".$_POST['tahun']."'</script>";
   }
  } else {
   $query = mysqli_query($kon,"update datatb set btapositif='".$_POST['btapositif']."',semuakasus='".$_POST['semuakasus']."',succesrate='".$_POST['succesrate']."',pengobatanlengkap='".$_POST['pengobatanlengkap']."' where id_tb='".$_POST['id_tb']."'");
   if($query){
    echo "<script>alert('Data TB Kabupaten berhasil diperbarui');\n document.location = 'lihat_tbkab.php?tahun=".$_POST['tahun']."'</script>";
   } else {
    echo mysqli_error($kon);
    echo "<script>alert('Terdapat Kesalahan dalam pembaruan data. ');\n document.location = 'lihat_tbkab.php?tahun=".$_POST['tahun']."'</script>";
   }
  }
}
include('header.php');
$forbid_flag = false;
?>
<form method="POST" action="form_tbkab.php" name="tbkab" class="form-horizontal">
 <input type="hidden" name="id_tb" value="<?= isset($row['id_tb'])?$row['id_tb']:'' ?>" />
 <input type="hidden" name="tahun" value="<?= isset($_GET['tahun'])?$_GET['tahun']:$row['tahun'] ?>" />
  <?php if(isset($_GET['edit'])) { ?>
  <h4>Kabupaten <?= $row['kabupaten'] ?> Tahun <?= $row['tahun'] ?></h4>
  <?php } else { ?>
  <h4>Data TB Kabupaten Tahun <?= $_GET['tahun'] ?></h4>
  <?php
   $dtb = [];
   $dtbquery = mysqli_query($kon, "select kabupaten from datatb where tahun ='".$_GET['tahun']."'");
   while($row=mysqli_fetch_assoc($dtbquery)){
    $dtb[$row['kabupaten']] = true;
   }
   $query = mysqli_query($kon, "select id_kabupaten, kabupaten from kabupaten");
   $kabcount = mysqli_num_rows($query);
   if(count($dtb) !== $kabcount){ 
  ?>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="kabupaten">Kabupaten</label>
   </div>
   <div class="col-sm-8">
    <select name="kabupaten" id="kabupaten" class="form-control">
    <?php
     while($row=mysqli_fetch_assoc($query)){ 
     ?>
     <option value="<?= $row['id_kabupaten'] ?>" <?= isset($dtb[$row['id_kabupaten']])?"disabled":"" ?> ><?= $row['kabupaten'] ?> <?= isset($dtb[$row['id_kabupaten']])?"(Sudah Ada)":"" ?></option>
    <?php } ?>
    </select>
   </div>
  </div>
  <?php } else { $forbid_flag = true; } 
  }
  if($forbid_flag == false){ 
  ?>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="btapositif">Jumlah BTA Positif</label>
   </div>
   <div class="col-sm-8">
    <input type="number" name="btapositif" id="btapositif" class="form-control" value="<?= isset($row['btapositif'])?$row['btapositif']:'' ?>" placeholder="BTA Positif" required />
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="semuakasus">Jumlah Seluruh Kasus</label>
   </div>
   <div class="col-sm-8">
    <input type="number" name="semuakasus" id="semuakasus" class="form-control" value="<?= isset($row['semuakasus'])?$row['semuakasus']:'' ?>" placeholder="Seluruh Kasus" required />
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="succesrate">Tingkat Kesuksesan</label>
   </div>
   <div class="col-sm-8">
    <input type="number" name="succesrate" id="succesrate" class="form-control" value="<?= isset($row['succesrate'])?$row['succesrate']:'' ?>" placeholder="Success Rate" required />
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="pengobatanlengkap">Jumlah Pengobatan Lengkap</label>
   </div>
   <div class="col-sm-8">
    <input type="number" name="pengobatanlengkap" id="pengobatanlengkap" class="form-control" value="<?= isset($row['pengobatanlengkap'])?$row['pengobatanlengkap']:'' ?>" placeholder="Pengobatan Lengkap" required />
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-12">
    <button type="submit" class="form-control btn btn-login" name="simpan">Simpan</button>
   </div>
  </div>
  <?php } else { ?>
  <h4>Data TB untuk seluruh Kabupaten telah dimasukkan.</h4>
  <a href="lihat_tbkab.php?tahun=<?= $_GET['tahun'] ?>" class="btn btn-primary btn-md"><span class="fa fa-chevron-left"></span> Kembali</a>
  <?php } ?>
</form>
<?php
include('footer.php');