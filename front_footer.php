    <footer class="footer">
        <div class="container" style="color:#fff;text-align:center">
            <p>&copy; <?= date('Y') ?> Dinas Kesehatan Provinsi Riau</p>
        </div>
    </footer>
  <script src="assets/js/jquery-3.2.1.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
    <?= $vws->get_inline() ?>
</body>
</html>