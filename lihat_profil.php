<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "Kelola Konten Profil";
$config['hal_aktif'] = "profil";
hak_akses(['admin'],TRUE);
include('header.php');
?>

<div class="col-sm-12">
  <div class="btn-group" style="margin:25px 5px">
   <a href="form_profil.php" class="btn btn-primary btn-md"><span class="fa fa-plus"></span> Tambah Konten</a>
  </div>
  <table class="table table-striped table-bordered">
   <thead>
    <tr>
     <th>Judul</th>
     <th>Aksi</th>
    </tr>
   </thead>
   <tbody>
    <?php 
    $query = mysqli_query($kon, "select id_halaman,judul from halaman");
    while($row=mysqli_fetch_assoc($query)){ ?>
    <tr>
     <td><?= $row['judul'] ?></td>
     <td><a href="profil.php?id=<?= $row['id_halaman'] ?>" target="_blank"><span class="fa fa-eye"></span> Lihat</a> | <a href="form_profil.php?edit=<?= $row['id_halaman'] ?>"><span class="fa fa-pencil"></span> Ubah</a> | <a href="hapus.php?table=halaman&id=<?= $row['id_halaman'] ?>" onclick="return confirm('Hapus halaman ini?');"><span class="fa fa-close"></span> Hapus</a>
    </tr>
    <?php } ?>
   </tbody>
  </table>
</div>
<?php
include('footer.php');