{
"type": "FeatureCollection",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
<?php
include('config/koneksi.php');
 
$query = "SELECT kabupaten.id_kabupaten, kabupaten.kabupaten, kabupaten.geometry, datatb.tahun FROM `datatb`, kabupaten where datatb.kabupaten = kabupaten.id_kabupaten";
$hasil = mysqli_query($kon, $query);
$h = [];
while($x = mysqli_fetch_array($hasil)){
 $h[$x['id_kabupaten']] = array(
  'type' => 'Feature',
  'properties' => array(
   'jenis' => 'kabupaten',
   'id_kabupaten' => $x["id_kabupaten"],
   'kabupaten' => $x["kabupaten"]
  ),
  'geometry' => array(
   'type' => "MultiPolygon",
   'coordinates' => json_decode($x['geometry'])
  )
 );
}

$hasil = mysqli_query($kon, "SELECT kabupaten.id_kabupaten, kabupaten.kabupaten, btapositif, semuakasus, succesrate, pengobatanlengkap, datatb.tahun FROM `datatb`, kabupaten where datatb.kabupaten = kabupaten.id_kabupaten");
while($x = mysqli_fetch_array($hasil)){
 $h[$x['id_kabupaten']]['properties']['btapositif'][$x['tahun']] = $x['btapositif'];
 $h[$x['id_kabupaten']]['properties']['semuakasus'][$x['tahun']] = $x['semuakasus'];
 $h[$x['id_kabupaten']]['properties']['succesrate'][$x['tahun']] = $x['succesrate'];
 $h[$x['id_kabupaten']]['properties']['pengobatanlengkap'][$x['tahun']] = $x['pengobatanlengkap'];
}
$count = 0;
foreach($h as $val){
 echo json_encode($val);
 $count++;
 if($count < count($h)){
  echo ",";
 }
}
?>
]
}