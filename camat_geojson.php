{
"type": "FeatureCollection",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
<?php
include('config/koneksi.php');

$query = "SELECT kecamatan.id_kecamatan, kecamatan.kecamatan, kecamatan.geometry, datatbkota.tahun FROM `datatbkota`, kecamatan where datatbkota.kecamatan = kecamatan.id_kecamatan";
$hasil = mysqli_query($kon, $query);
$h = [];
while($x = mysqli_fetch_array($hasil)){
 $h[$x['id_kecamatan']] = array(
  'type' => 'Feature',
  'properties' => array(
   'jenis' => 'kecamatan',
   'id_kecamatan' => $x["id_kecamatan"],
   'kabupaten' => $x["kecamatan"]
  ),
  'geometry' => array(
   'type' => "MultiPolygon",
   'coordinates' => json_decode($x['geometry'])
  )
 );
}

$hasil = mysqli_query($kon, "SELECT kecamatan.id_kecamatan, kecamatan.kecamatan, btapositif, semuakasus, succesrate, pengobatanlengkap, datatbkota.tahun FROM `datatbkota`, kecamatan where datatbkota.kecamatan = kecamatan.id_kecamatan");
while($x = mysqli_fetch_array($hasil)){
 $h[$x['id_kecamatan']]['properties']['btapositif'][$x['tahun']] = $x['btapositif'];
 $h[$x['id_kecamatan']]['properties']['semuakasus'][$x['tahun']] = $x['semuakasus'];
 $h[$x['id_kecamatan']]['properties']['succesrate'][$x['tahun']] = $x['succesrate'];
 $h[$x['id_kecamatan']]['properties']['pengobatanlengkap'][$x['tahun']] = $x['pengobatanlengkap'];
}
$count = 0;
foreach($h as $val){
 echo json_encode($val);
 $count++;
 if($count < count($h)){
  echo ",";
 }
}

?>
]
}