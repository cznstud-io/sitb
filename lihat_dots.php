<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "Kelola Data DOTS Pekanbaru";
$config['hal_aktif'] = "dots";
hak_akses(['admin'],TRUE);
ob_start();
?>
<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/jquery.dataTables_themeroller.min.css" rel="stylesheet"/>
<?php
$vws->set_inline(ob_get_clean());
include('header.php');
$vws->reset_inline();
?>
<style>
 hr{
  border: 1px solid #7a7a7a;
  margin: 5px;
 }
 .print-only { display: none }
 @media print{
  nav, #content-title, .hidden-print { display: none }
  .print-only { display: block }
 }
</style>
<div class="col-sm-12">

 <div class="btn-group hidden-print" style="margin:25px 5px">
  <a href="form_dots.php"class="btn btn-primary btn-md"><span class="fa fa-plus"></span> Tambah Klinik</a>
  <button type="button" class="btn btn-primary btn-md" id="printhasil"><span class="fa fa-file-pdf-o"></span> Unduh Laporan</a>
 </div>

 <table class="table table-striped table-bordered table-responsive" id="klinik">
  <thead>
   <tr>
    <th>Nama Klinik</th>
    <th>Koordinat</th>
    <th>Jenis</th>
    <th>Aksi</th>
   </tr>
  </thead>
  <tbody>
   <?php 
   $query = mysqli_query($kon, "select * from dots");
   $rows = [];
   if(mysqli_num_rows($query) !== 0){
    while($row=mysqli_fetch_assoc($query)){ 
    $rows[] = array(
      'kli'=> $row['namaklinik'],
      'koord'=> "(".$row['latitude'].", ".$row['longitude'].")",
      'jns'=> $config['jenis_klinik'][$row['jenis']],
    );
    ?>
    <tr>
     <td><?= $row['namaklinik'] ?></td>
     <td>(<?= $row['latitude'].", ".$row['longitude'] ?>)</td>
     <td><?= $config['jenis_klinik'][$row['jenis']] ?></td>
     <td class="hidden-print text-center"><a href="form_dots.php?edit=<?= $row['idklinik'] ?>"><span class="fa fa-pencil"></span> Ubah</a> | <a href="hapus.php?table=dots&id=<?= $row['idklinik'] ?>"onclick="return confirm('Hapus data ini?')"><span class="fa fa-close"></span> Hapus</a></td>
    </tr>
    <?php }
    } else {
     ?>
     <tr>
      <td colspan="5">Tidak Ada Data</td>
     </tr>
    <?php } ?>
  </tbody>
 </table>
</div>
<?php
ob_start();
?>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap.min.js"></script>
<script src="assets/js/jspdf.min.js"></script>
<script src="assets/js/jspdf.plugin.autotable.min.js"></script>
<script>
  var cols = [
    {title: "Nama Klinik", dataKey: 'kli'},
    {title: "Koordinat", dataKey: 'koord'},
    {title: "Jenis", dataKey: 'jns'}
  ];
  var rows = <?= json_encode($rows) ?>;
  $(document).ready(function() {
    $("#printhasil").on('click', function() {
    var doc = new jsPDF({
     orientation: 'l',
     unit: 'pt',
     format: 'a4'
    });
    doc.autoTable(cols, rows, {
     theme: 'grid',
     margin: {top: 100},
     styles: {halign:'center'},
     addPageContent: function(data) {
       doc.setFontSize(14);
       doc.text("Dinas Kesehatan Provinsi Riau", 40, 30);
       doc.text("Laporan Titik DOTS Kota Pekanbaru", 320, 70);
       doc.setLineWidth(1.5);
       doc.setDrawColor(50,50,50);
       doc.line(40,80,800,80);
     }
    });
    doc.save("laporan-data.pdf");
   });
   $("#klinik").DataTable();
  });
  
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');